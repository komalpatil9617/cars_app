-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 04, 2022 at 10:11 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cardb`
--

-- --------------------------------------------------------

--
-- Table structure for table `car`
--

CREATE TABLE `car` (
  `id` int(50) NOT NULL,
  `model` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `company` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `mileage` varchar(255) NOT NULL,
  `sitno` varchar(255) NOT NULL,
  `fuel` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `car`
--

INSERT INTO `car` (`id`, `model`, `image`, `company`, `type`, `mileage`, `sitno`, `fuel`) VALUES
(2, 'hh', 'upload/car.png', 'Honda Cars.', 'Sedan', '05', '6', 'Electric'),
(3, 'Mahindra XUV700', 'upload/carimage1.jpg', 'Mahindra & Mahindra.', 'Sedan', '28.40', '5', 'Diesel'),
(4, 'tt', 'upload/carimage1.jpg', 'Honda Cars.', 'Sedan', '90', '7', 'CNG'),
(8, 'mm', 'upload/carbanner.jpg', 'Tata Motors', 'SUV', '7', '6', 'Diesel'),
(9, 'rr', 'upload/carbanner.jpg', 'Tata Motors', 'SUV', '05', '6', 'Diesel'),
(10, 'pp', 'upload/carbanner.jpg', 'Tata Motors', 'Sedan', '09', '6', 'Diesel'),
(11, 'll', 'upload/carimage1.jpg', 'Tata Motors', 'SUV', '7', '6', 'Diesel'),
(12, 'zz', 'upload/car.jpeg', 'Tata Motors', 'SUV', '88', '6', 'Diesel'),
(13, 'pp', 'upload/carbanner.jpg', 'Tata Motors', 'Sedan', '28.40', '6', 'Diesel');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `car`
--
ALTER TABLE `car`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `car`
--
ALTER TABLE `car`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
