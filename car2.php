<html>
<head>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

<link rel="stylesheet" href="style.css">

</head>
<body>


<?php

include'connection.php';


if($_SERVER['REQUEST_METHOD'] == 'POST')
{

$model= $_POST['model'];
$image =  $_FILES['image'];
$company =  $_POST['company'];
$type= $_POST['type'];
$mileage =  $_POST['mileage'];
$sitno =  $_POST['sitno'];
$fuel =  $_POST['fuel'];

$filename=$image['name'];
$filepath=$image['tmp_name'];
$fileerror=$image['error'];

 if($fileerror==0)
{
	$destfile= 'upload/'.$filename;
	
	move_uploaded_file($filepath,$destfile);
	
	$insertquery= "INSERT INTO `car` (`model`, `image`, `company`, `type`, `mileage`, `sitno`, `fuel`) VALUES ( '$model', '$destfile', '$company', '$type', '$mileage', '$sitno', '$fuel')";
	
	$query= mysqli_query($conn,$insertquery);
	if($query)
	{
		echo"data inserted successfully";
		header('location:carlist.php');
		
		
	}
	else
	{
		echo"data not inserted";
		
		
	}


}
}
else
{
	echo"button not selected";
	
}


?>
























<div class="container mt-xl-5 mb-5">
    <div class="d-flex justify-content-center pt-5">
        <h2 class="font-weight-bold">Fill The Form</h2>
    </div>
    <div class="d-flex justify-content-center text-muted">It is a never ending battle of making your cars better and also trying to be better yourself.</div>
    <div class="d-md-flex flex-md-row justify-content-center py-4">
        
    </div>
    <div class="d-flex flex-row justify-content-center">
        <form class="w-xl-50 w-lg-75" action="car2.php" enctype="multipart/form-data" method="post">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group"> <label for="model">Car Model</label>
                            <div class="input-field"> <span class="fa fa-user-o p-2 border-right"></span> <input type="text" class="form-control" id="inputEmail4" name="model"></div>
                        </div>
                        <div class="form-group"> <label for="image">Car Image</label>
                            <div class="input-field"> <span class="fa fa-envelope-o p-2"></span> <input type="file" class="form-control" id="inputPassword4" name="image"> </div>
                        </div>
                        <div class="form-group"> <label for="company">Car Company:</label>
                            <div class="input-field"> <span class="fa fa-mobile p-2"></span> <select id="company" name="company" class="form-select">
    <option value="Maruti Suzuki">Maruti Suzuki</option>
    <option value="Hyundai India">Hyundai India</option>
    <option value="Tata Motors">Tata Motors</option>
    <option value="Honda Cars."> Honda Cars</option>
	<option value="Mahindra & Mahindra."> Mahindra & Mahindra.</option>
  </select></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                         <div class="form-group"> <label for="type">Car Type:</label>
                            <div class="input-field"> <span class="fa fa-user-o p-2 border-right"></span><select id="type" name="type" class="form-select">
    <option value="Hatchback">Hatchback</option>
    <option value="Sedan">Sedan</option>
    <option value="SUV">SUV</option>
    <option value="Luxury">Luxury</option>
	</select> </div>
                        </div>
                        <div class="form-group"> <label for="mileage">Car Mileage</label>
                            <div class="input-field"> <span class="fa fa-envelope-o p-2"></span> <input type="text" class="form-control" id="inputCity" name="mileage"> </div>
                        </div>
                        <div class="form-group"> <label for="sitno">Number Of Sits:</label>
                            <div class="input-field"> <span class="fa fa-mobile p-2"></span> <select id="sitno" class="form-select" name="sitno">
       <option value="5">5</option>
    <option value="6">6</option>
    <option value="7">7</option>
    </select></div>
                        </div>
						
						
						
						
						
						
						
						
                    </div>
                    </div>
					<div class="form-group"> <label for="fuel">Fuel Type:</label>
                            <div class="input-field"> <span class="fa fa-mobile p-2"></span> <select id="fuel" class="form-select" name="fuel">
       <option value="Petrol">Petrol</option>
    <option value="Diesel">Diesel</option>
    <option value="CNG">CNG</option>
	<option value="Electric">Electric</option>
    </select></div>
                        </div>
						
                    <div class="d-flex flex-row justify-content-center w-100"> <button type="submit" class="btn btn-primary btn-block mb-3" >Submit</button> </div>
                </div>
            </div>
        </form>
    </div>
</div>
</body>

</html>